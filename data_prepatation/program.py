from io import StringIO

import numpy as np
import pandas as pd

columns = ["customerID", "gender", "SeniorCitizen", "Partner", "Dependents", "tenure", "PhoneService",
           "MultipleLines", "InternetService", "OnlineSecurity", "OnlineBackup", "DeviceProtection",
           "TechSupport", "StreamingTV", "StreamingMovies", "Contract", "PaperlessBilling", "PaymentMethod",
           "MonthlyCharges", "TotalCharges", "Churn"]


class DataPreprocessor(object):

    def __init__(self, columns):
        """

        :param columns:
        """
        self.columns = columns
        print('processor created for', columns)

    def load(self, csv_txt):
        """
        Taking csv txt and loading returns dataframe
        if data correct or throws error
        :return: pandas data frame
        """
        df = pd.read_csv(StringIO(csv_txt))

        if not self.__check_columns(df):
            raise Exception('Missing columns')

        self.__drop_nonrelevant_col(df)

        return df

    def __check_columns(self, df):
        """
        Checking if all required columns exists
        :param df:
        :return:
        """
        for col in columns:
            if col not in df.columns:
                print("Missing columns: ", col)
                return False

        return True

    def __drop_nonrelevant_col(self, df):
        """
        Removes columns not in list
        :param df:
        :return:
        """
        to_remove = []
        for col in df.columns:
            if col not in columns:
                to_remove.append(col)

        df.drop(columns=[to_remove], inplace=True)


if __name__ == "__main__":
    print('starting loader')
    preprocessor = DataPreprocessor(columns)
    try:
        with open('./test_data/WA_Fn-UseC_-Telco-Customer-Churn.csv') as f:
            csv_txt = f.read()

        df = preprocessor.load(csv_txt)

    except Exception as exc:
        print("Error occured: ", exc)
