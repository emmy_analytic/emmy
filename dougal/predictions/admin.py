from django.contrib import admin

# Register your models here.

from .models import UserGroup, ServiceUser,ServiceActions

admin.site.register(UserGroup)
admin.site.register(ServiceUser)
admin.site.register(ServiceActions)