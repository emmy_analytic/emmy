BEGIN;
--
-- Create model ServiceActions
--
CREATE TABLE "predictions_serviceactions" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "action_name" varchar(150) NOT NULL, "type_change" date NOT NULL, "result" integer NOT NULL);
--
-- Create model ServiceUser
--
CREATE TABLE "predictions_serviceuser" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "name" varchar(100) NOT NULL, "oid" integer NOT NULL, "register_date" date NOT NULL, "status" varchar(2) NOT NULL, "is_paid" bool NOT NULL, "type_change" date NOT NULL, "status_change" date NOT NULL, "actions_count" integer NOT NULL);
--
-- Create model UserGroup
--
CREATE TABLE "predictions_usergroup" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "name" varchar(30) NOT NULL, "description" varchar(300) NOT NULL, "ogid" integer NOT NULL);
--
-- Add field user_group to serviceuser
--
ALTER TABLE "predictions_serviceuser" RENAME TO "predictions_serviceuser__old";
CREATE TABLE "predictions_serviceuser" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "name" varchar(100) NOT NULL, "oid" integer NOT NULL, "register_date" date NOT NULL, "status" varchar(2) NOT NULL, "is_paid" bool NOT NULL, "type_change" date NOT NULL, "status_change" date NOT NULL, "actions_count" integer NOT NULL, "user_group_id" integer NOT NULL REFERENCES "predictions_usergroup" ("id") DEFERRABLE INITIALLY DEFERRED);
INSERT INTO "predictions_serviceuser" ("id", "name", "oid", "register_date", "status", "is_paid", "type_change", "status_change", "actions_count", "user_group_id") SELECT "id", "name", "oid", "register_date", "status", "is_paid", "type_change", "status_change", "actions_count", NULL FROM "predictions_serviceuser__old";
DROP TABLE "predictions_serviceuser__old";
CREATE INDEX "predictions_serviceuser_user_group_id_f9b12c49" ON "predictions_serviceuser" ("user_group_id");
--
-- Add field user_id to serviceactions
--
ALTER TABLE "predictions_serviceactions" RENAME TO "predictions_serviceactions__old";
CREATE TABLE "predictions_serviceactions" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "action_name" varchar(150) NOT NULL, "type_change" date NOT NULL, "result" integer NOT NULL, "user_id_id" integer NOT NULL REFERENCES "predictions_usergroup" ("id") DEFERRABLE INITIALLY DEFERRED);
INSERT INTO "predictions_serviceactions" ("id", "action_name", "type_change", "result", "user_id_id") SELECT "id", "action_name", "type_change", "result", NULL FROM "predictions_serviceactions__old";
DROP TABLE "predictions_serviceactions__old";
CREATE INDEX "predictions_serviceactions_user_id_id_0c09ef9e" ON "predictions_serviceactions" ("user_id_id");
COMMIT;