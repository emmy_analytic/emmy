from django.db import models

class UserGroup(models.Model):
	name = models.CharField(max_length=30)
	description = models.CharField(max_length=300)
	# original group id
	ogid = models.IntegerField()
	
class ServiceUser(models.Model):
	USER_STATUS = (
		('AC','Active'),
		('DS','Destroyed'),
		('DA', 'Deactivated'),
		('SU','Suspended')
	)

    # email or login
	name = models.CharField(max_length=100)
	# original user id
	oid = models.IntegerField()
	# fk to user group
	user_group = models.ForeignKey(UserGroup, on_delete=models.CASCADE)
	# user registration date
	register_date = models.DateField()
	# user latest status
	status = models.CharField(max_length=2, choices=USER_STATUS)
	# paid user or not
	is_paid = models.BooleanField(default=False)
	# last type change time
	type_change = models.DateField()
	# last status change
	status_change = models.DateField()
	# placegolde for env count
	actions_count = models.IntegerField()
	
class ServiceActions(models.Model):
	# id of user that makes action
	user_id = models.ForeignKey(UserGroup, on_delete=models.CASCADE)
	# type of action is string represantion
	action_name = models.CharField(max_length=150)
	# last type change time
	action_time = models.DateField()
	# code of result
	result = models.IntegerField()
	

	