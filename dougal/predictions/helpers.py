from django.core.files.storage import FileSystemStorage
import csv
from .models import UserGroup

DATA_FOLDER = 'predictions/data/'

def handle_uploaded_file(name, f):
	fs = FileSystemStorage(location=DATA_FOLDER)
	filename = fs.save("{}.csv".format(name), f)
	
def save_users_to_db():
	print('save_users_to_db')
	fs = FileSystemStorage(location=DATA_FOLDER)
	file = fs.open("user_file.csv", 'r')
	creader = csv.reader(file, delimiter=',',quotechar='|')
	
	# skipping header and creating models
	is_header = False
	for row in creader:
		if not is_header:
			is_header = True
		else:
			group = UserGroup(ogid=row[0], name=row[1], description=row[2])
			group.save()