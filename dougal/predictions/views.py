from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth import authenticate, logout, login
from django.contrib.auth.decorators import login_required
from .forms import UploadFileForm
from .helpers import handle_uploaded_file, save_users_to_db


#############################################################################
# Views Logic
#############################################################################

def index(request):
    return render(request, 'predictions/index.html')


@login_required(login_url='/predictions/login/')
def import_data(request):
    if request.method == 'GET':
        return render(request, 'predictions/import.html')
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            if not save_raw_data():
                return render(request, 'predictions/import.html', {'error': 'Error, Please check file stracture'})

            save_users_to_db()
            return render(request, 'predictions/import.html', {'result': 'Data was saved! Loading to database...'})
        else:
            return render(request, 'predictions/import.html', {'error': 'Error, check all fields are not empty!'})

    return render(request, 'predictions/import.html')


def login_view(request):
    print('login')
    if request.method == 'GET':
        return render(request, 'predictions/login.html')

    if request.method == 'POST':
        username = request.POST.get('inputLogin')
        password = request.POST.get('inputPassword')
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('import')
        else:
            return render(request, 'predictions/login.html', {'error': 'Login or password is wrong!'})


def logout_view(request):
    logout(request)
    return redirect('index')


#############################################################################
# Helpers Logic
#############################################################################

def save_raw_data(request):
    try:
        handle_uploaded_file('group_file', request.FILES['group_file'])
        handle_uploaded_file('user_file', request.FILES['user_file'])
        handle_uploaded_file('action_file', request.FILES['action_file'])

        return True
    except Error as err:
        print('save_raw_data() - error: ', err)
        return False
