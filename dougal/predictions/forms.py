from django import forms


class UploadFileForm(forms.Form):
	delimiter = forms.CharField(max_length=2)
	group_file = forms.FileField()
	user_file = forms.FileField()
	action_file = forms.FileField()
	