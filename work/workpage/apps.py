from django.apps import AppConfig


class WorkpageConfig(AppConfig):
    name = 'workpage'
