from django.contrib import admin
from workpage.models import UserProfile

# Register your models here.
admin.site.register(UserProfile)
