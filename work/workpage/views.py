from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from .forms import DocumentForms
from django.views.decorators.csrf import csrf_exempt
from django.core.files.storage import FileSystemStorage
# Create your views here.

@login_required
def dashboard(request):
    args = {"user": request.user}
    return render(request, "dashboard.html", args)


def register(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("/login/")
    else:
        form = UserCreationForm()

        args = {"form": form}
        return render(request, "reg_form.html", args)


@login_required
@csrf_exempt
def model_form_upload(request):
    if request.method == 'GET':
        return render(request, 'importfile.html', {})
    if request.method == 'POST':
        file = request.FILES['customFile']
        fs = FileSystemStorage()
        filename = fs.save(file.name, file)

        return render(request, 'importfile.html', {"message": "File was saved"})

