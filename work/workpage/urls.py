
from django.urls import path
from . import views
from django.contrib.auth.views import login, logout
from workpage.forms import LoginForm

urlpatterns = [
    path("login/", login, {"template_name": "signIn.html", "authentication_form": LoginForm}, name="signIn"),
    path("logout/", logout, {"template_name": "signOut.html"}, name="signOut"),
    path("dashboard/", views.dashboard, name="dashboard"),
    path("register/", views.register, name="register"),
    path("dashboard/importfile/", views.model_form_upload, name="importfile"),


]
