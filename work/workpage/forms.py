from django import forms
from .models import Document
from django.contrib.auth import models
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.forms import UserCreationForm



class LoginForm(AuthenticationForm):
    username = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'from-control', 'placeholder': ''}
    ), label='Username')
    password = forms.CharField(widget=forms.PasswordInput(
        attrs={'class': 'from-control', 'placeholder': ''}
    ), label='Password')

class RegistrationForm(UserCreationForm):
    email = forms.EmailField(required=True)
    class Meta:
        model = User
        fields = (
            'username',
            'first_name',
            'last_name',
            'email',
            'password1',
            'password2'

        )
    def save(self, commit=True):
        user = super(RegistrationForm, self).save(commit=False)
        user.first_name = self.cleaned_data["first_name"]
        user.last_name = self.cleaned_data["last_name"]
        user.email = self.cleaned_data["email"]

        if commit:
            user.save()
        return user


class DocumentForms(forms.ModelForm):
    class Meta:
        model = Document
        fields = (
            'description', 'document',
        )




